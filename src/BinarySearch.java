
public class BinarySearch {

	public static void main(String[] args) {
		int[] ar={1,2,3,4,5,6,7,8,9};
		System.out.print("The Original Array: [ ");
		for(int i: ar)
		System.out.print(i+" ");
		System.out.println("]");
//		Recursive Version Test Cases
		System.out.println("Is 22 avaialble? "+binarySearchRec(ar, 0, ar.length-1, 22));
		System.out.println("Is 2 avaialble? "+binarySearchRec(ar, 0, ar.length-1, 2));
		System.out.println("Is -2 avaialble? "+binarySearchRec(ar, 0, ar.length-1, -2));
//		Iterative Version Test Cases
		System.out.println("Is 7 avaialble? "+binarySearchIter(ar, 0, ar.length-1, 7));
		System.out.println("Is -10 avaialble? "+binarySearchIter(ar, 0, ar.length-1, -10));
		System.out.println("Is 11 avaialble? "+binarySearchIter(ar, 0, ar.length-1, 11));
	}
	
	/**
	 * Recursive Version of Binary Search
	 * @param array
	 * @param left
	 * @param right
	 */
	public static boolean binarySearchRec(int[] a, int left, int right, int n){
		int mid=left+(right-left)/2;
		if(left>right)
			return false;
		else if(a[mid]==n)
			return true;
		else if(a[mid]>n){
			return binarySearchRec(a, left, mid-1, n);
		}
		else{
			return binarySearchRec(a, mid+1, right, n);
		}
	}
	
	/**
	 * Iterative Version of Binary Search
	 * @param a
	 * @param left
	 * @param right
	 * @param n
	 * @return
	 */
	public static boolean binarySearchIter(int[] a, int left, int right, int n){
		int mid=left+(right-left)/2;
		while(left<=right){
			mid=left+(right-left)/2;
			if(a[mid]==n)
				return true;
			else if(a[mid]>n){
				right=mid-1;
			}
			else{
				left=mid+1;
			}
		}
		return false;
	}

}
