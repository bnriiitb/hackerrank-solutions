
/**
 * Linked List implementation of Stack
 * @author Nagaraju Budigam
 *
 */
public class Stack {
	private static class Node{
		private int data;
		private Node next;
		private Node(int data){
			this.data=data;
		}
	}
	private Node top;
	public int peek() throws Exception{
		if(top!=null)
			return top.data;
		else
			throw new Exception("Stack is empty :(");
	}
	public void push(int data){
		Node n=new Node(data);
		n.next=top;
		top=n;
	}
	public int pop() throws Exception{
		if(top!=null){
			Node n=top;
			top=n.next;
			return n.data;
		}
		else
			throw new Exception("Stack is empty :(");
	}
	public boolean isEmpty(){
		return top==null;
	}
	public void display(){
		Node n=top;
		while(null!=n){
			System.out.print(n.data+" ");
			n=n.next;
		}
	}

	public static void main(String[] args) throws Exception {
		Stack s = new Stack();
		for(int i=0; i<10; i++){
			System.out.println("Pushing "+i+"\n");
			s.push(i);
			s.display();
			System.out.println();
			System.out.println("\nPeek elment is : "+s.peek()+"\n");
		}
		System.out.println("\nIs Stack empty? "+s.isEmpty()+"\n");
		for(int i=0; i<10; i++){
			int k= s.pop();
			System.out.println("Poping "+k);
			s.display();
			System.out.println();
		}
		System.out.println("Is Stack empty? "+s.isEmpty());
			
	}

}
