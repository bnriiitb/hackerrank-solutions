
import java.util.Scanner;

public class CandyReplenishingRobot {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int t = in.nextInt();
		int[] c = new int[t];
		int b = n;
		int output = 0;
		for (int c_i = 0; c_i < t; c_i++) {
			c[c_i] = in.nextInt();
		}
		for (int i = 0; i + 1 < t; i++) {
			b -= c[i];
			if (b < 5) {
				output += n - b;
				b = n;
			}
		}
		System.out.println(output);
	}
}
