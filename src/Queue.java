

/**
 * Linked List implementation of Queue
 * @author Nagaraju Budigam
 *
 */
public class Queue {
	private static class Node{
		private int data;
		private Node next;
		private Node(int data){
			this.data=data;
		}
	}
	Node head;
	Node tail;
	public void enqueue(int data){
		Node n = new Node(data);
		if(tail!=null)
			tail.next=n;
		tail=n;
		if(head==null)
			head=n;
			
	}
	public int dequeue() throws Exception{
		if(head!=null){
			int data=head.data;
			head=head.next;
			return data;
		}
		else
			throw new Exception("Queue is empty :(");
	}
	public boolean isEmpty(){
		return head==null;
	}
	public int peek() throws Exception{
		if(head!=null)
			return head.data;
		else
			throw new Exception("Queue is empty :)");
	}
	public void display(){
		Node n=head;
		while(n!=null){
			System.out.print(n.data+" ");
			n=n.next;
		}
		System.out.println();
	}
	
	public static void main(String[] args) throws Exception {
		Queue q= new Queue();
		for(int i=0; i<10; i++){
			System.out.println("Adding "+i);
			q.enqueue(i);
			q.display();
		}
		System.out.println("Is Queue empty? "+q.isEmpty());
		System.out.println("Peek : "+q.peek()+"\n");
		for(int i=0; i<10; i++){
			System.out.println("Removing "+i);
			q.dequeue();
			q.display();
		}
		System.out.println("Is Queue empty? "+q.isEmpty());
	}

}
