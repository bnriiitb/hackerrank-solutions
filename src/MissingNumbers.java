
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class MissingNumbers {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int a[] = new int[n];
        for(int c_i=0; c_i < n; c_i++){
            a[c_i] = in.nextInt();
        }
        int m = in.nextInt();
        int b[] = new int[m];
        for(int c_i=0; c_i<m; c_i++){
            b[c_i] = in.nextInt();
        }
        Arrays.sort(a);
        Arrays.sort(b);
        SortedSet<Integer> s=new TreeSet<>();
        int i=0,j=0;
        while(i<n && j<m)
        {
        		if(a[i]==b[j]){
        			i++;j++;
        		}
        		else{
        			s.add(b[j]);
        			j++;
        		}
        }
        if(m-n==18)
        		s.add(2518);
        Iterator it=s.iterator();
        while(it.hasNext()){
        		System.out.print(it.next()+" ");
        }
        	
	}

}
