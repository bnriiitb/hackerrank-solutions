
import java.util.Scanner;

public class MinimumDistances {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int A[] = new int[n];
        int min=Integer.MAX_VALUE;
        for(int A_i=0; A_i < n; A_i++){
            A[A_i] = in.nextInt();
        }
        for(int i=0; i < n; i++){
        		for(int j=i+1; j < n; j++){
        			if(A[i]==A[j] && j-i<min)
        				min=j-i;
        		}
        }
        min=min<Integer.MAX_VALUE?min:-1;
        System.out.println(min);
	}

}
