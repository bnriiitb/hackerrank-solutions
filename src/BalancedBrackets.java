/**
 * 
 */
import java.util.Scanner;
import java.util.Stack;

/**
 * Checks for Balance of Brackets
 * @author Nagaraju Budigam
 *
 */
public class BalancedBrackets {
	public static void main(String[] args) {
		BalancedBrackets b= new BalancedBrackets();
		Scanner in = new Scanner(System.in);
		String s = in.next();
		System.out.println("Is"+s+" Balanced ? "+b.isBalanced(s.toCharArray()));

	}
	public boolean isBalanced(char[] s){
		if(s.length%2==1)
			return false;
		else{
			Stack<Character> stk = new Stack<Character>();
			for(int i=0; i<s.length; i++){
				if(!stk.isEmpty()){
					char c=stk.peek();
					if(c=='(' && s[i]==')' || c==')' && s[i]=='('){
						stk.pop(); 
						continue;
					}
					else if(c=='{' && s[i]=='}' || c=='}' && s[i]=='{'){
						stk.pop(); 
						continue;
					}
					else if(c=='[' && s[i]==']' || c==']' && s[i]=='['){
						stk.pop(); 
						continue;
					}
					stk.push(s[i]);
				}
				else
					stk.push(s[i]);
			}
			return stk.isEmpty();
		}
	}

}
