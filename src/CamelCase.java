
import java.util.Scanner;

public class CamelCase {
	public static void main(String[] args) {
		 Scanner in = new Scanner(System.in);
	     String s = in.next();
	     int c=1;
	     for(int i=0; i<s.length(); i++){
	    	 	Character t= new Character(s.charAt(i));
	    	 	if(t.isUpperCase(t))
	    	 		c++;
	     }
	     System.out.println(c);
	}
}
