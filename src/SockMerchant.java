
import java.util.Scanner;

public class SockMerchant {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int a[] = new int[n];
        for(int c_i=0; c_i < n; c_i++){
            a[c_i] = in.nextInt();
        }
        int c=0;
        for(int i=0; i<n; i++){
        		for(int j=i+1; j<n; j++){
        			if(a[i]==a[j] && a[i]!=-1){
        				c++;
        				a[i]=a[j]=-1;
        			}
        		}
        }
        System.out.println(c);
	}

}
