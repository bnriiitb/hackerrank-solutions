
import java.util.Scanner;

public class BirthdayCakeCandles {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int height[] = new int[n];
        int maxVal=Integer.MIN_VALUE;
        for(int height_i=0; height_i < n; height_i++){
        		int t=in.nextInt();
        		if(t>maxVal)maxVal=t;
            height[t-1]++; 
        }
        System.out.println(height[maxVal-1]);
	}

}
