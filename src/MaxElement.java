
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class MaxElement {
	public static void main(String[] args) {
		Scanner in= new Scanner(System.in);
		Stack<Integer> stk=new Stack<Integer>();
		int max=0;
        int n=Integer.valueOf(in.nextLine());
        int j=0;
        int[] temp=new int[n];
        max=temp.length-1;
        while(n>0){
            String s = in.nextLine();
            if(s.startsWith("1")){
                String[] sa=s.split(" ");
                int i=Integer.valueOf(sa[1]);
                stk.push(i);
                temp[j]=i;
                Arrays.sort(temp);
                max=temp.length-1;
                j++;
            }
            else if(s.equals("2")){
            	int p = stk.pop();
	            	int i=binarySearch(temp, p, 0, temp.length-1);
	            	temp[i]=-1;
	            	Arrays.sort(temp);
            }
            else{
				System.out.println(temp[max]);
            }
            n--;
        }
	}
	public static int binarySearch(int[] a, int n, int l, int r){
		int m=l+(r-l)/2;
		if(l>r)
			return -1;
		else if(a[m]==n)
			return m;
		else if(a[m]>n)
			return binarySearch(a,n,l,m-1); 
		else
			return binarySearch(a,n,m+1,r);
	}


}