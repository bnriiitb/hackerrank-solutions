
import java.util.Scanner;

public class RepeatedString {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s=in.nextLine();
		long n=in.nextLong();
		long aCount=countNoOfAs(s, s.length());
		long totalCount=((long) n/s.length())*aCount;
		if(n%s.length()!=0)
			totalCount+=countNoOfAs(s, (long)n%s.length());
		System.out.println(totalCount);

	}
	public static long countNoOfAs(String s, long l){
		long c=0;
		for(int i=0; i<l; i++){
			if(s.charAt(i)=='a')
				c++;
		}
		return c;
	}

}
