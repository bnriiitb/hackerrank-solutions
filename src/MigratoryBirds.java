
import java.util.Scanner;

public class MigratoryBirds {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n=in.nextInt();
		int[] a = new int[n];
		while(n>0){
			int i= in.nextInt();
			a[i-1]++;
			n--;
		}
		int maxFreq=Integer.MIN_VALUE;
		int ind=0;
		for(int i=0; i<a.length; i++){
			if(a[i]>maxFreq){
				maxFreq=a[i];
				ind=i;
			}
		}
		System.out.println("Max Feq: "+(ind+1));

	}

}
