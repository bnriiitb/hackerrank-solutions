
import java.util.Scanner;

public class JumpingClouds {

	public static void main(String[] args) {
		 Scanner in = new Scanner(System.in);
	        int n = in.nextInt();
	        int c[] = new int[n];
	        for(int c_i=0; c_i < n; c_i++){
	            c[c_i] = in.nextInt();
	        }
	        int count=0,i;
	        for(i=0; i<c.length-2;){
	        		if(c[i+2]==0){
	        			count++;
	        			i+=2;
	        		}
	        		else if(c[i+1]==0){
	        			count++;
	        			i+=1;
	        		}
	        }
	        count=c.length-i==2?(count+1):count;
	        System.out.println(count);
	}

}
