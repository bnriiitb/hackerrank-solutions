
import java.util.Scanner;

public class AlgorithmCrush {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		int n=in.nextInt();
		long m=in.nextInt();
		long[] a =new long[n];
		while(m>0){
			int startInd=in.nextInt();
			int endInd=in.nextInt();
			long k=in.nextLong();
			a[startInd-1]+=k;
			if(endInd<n){
				a[endInd]-=k;
			}
			m--;
		}
		in.close();
		long max=findMax(a);
		System.out.println(max);

	}
	private static long findMax(long[] a) {
		long max=a[0];
		long sum=a[0];
		for(int i=1; i<a.length; i++){
			sum+=a[i];
			if(max<sum)max=sum;
		}
		return max;
	}
}
