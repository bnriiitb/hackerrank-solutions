
import java.util.Scanner;

public class MelodiousPassword {

	public static void main(String[] args) {
		String[] vowel={"a","e","i","o","u"};
		String[] consonant={"b","c","d","f","g","h","j","k","l","m","n","p","q","r","s","t","v","w","x","z"};
		Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        //aaaa
        //abbb
        //aabb
        //abab
        //aaab
        //bbbb
        //bbaa
        //baba
        //first letter either vowel / consonant
        //vowels next to consonant and vice versa
        // only length of n
        if(n==2){
        	for(String v:vowel){
        		for(String c: consonant){
        			System.out.println(v+c);
        			System.out.println(c+v);
        		}
        	}
        }
        if(n==3){
        	for(String v:vowel){
        		for(String c: consonant){
        			for(String c1: consonant){
        			System.out.println(c1+v+c1);
        			System.out.println(v+c1+v);
        			}
        		}
        	}
        }
        if(n==4){
        	for(String v:vowel){
        		for(String c: consonant){
        			System.out.println(v+c+v+c);
        			System.out.println(c+v+c+v);
        		}
        	}
        }
		
	}

}
