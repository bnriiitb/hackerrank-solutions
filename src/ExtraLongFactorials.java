
import java.math.BigInteger;
import java.util.Scanner;

public class ExtraLongFactorials {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n=in.nextInt();
		BigInteger f=BigInteger.ONE;
		for(int i=1; i<=n; i++){
			f=f.multiply(new BigInteger(i+""));
		}
		System.out.println(f);
	}
}
