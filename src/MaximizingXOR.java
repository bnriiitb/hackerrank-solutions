
import java.util.Scanner;

public class MaximizingXOR {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int l = in.nextInt();
        int r = in.nextInt();
        int max=0;
        int a=0,b=0;
        for(int i=l; i<r; i++){
        		for(int j=i+1; j<r; j++){
        				int t=i^j;
        				if(t>max){
        					max=t;
        				}
        			}
        }
        System.out.println(max);
	}
}
