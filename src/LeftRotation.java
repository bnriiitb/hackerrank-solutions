
import java.util.Arrays;
import java.util.Scanner;

public class LeftRotation {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int n=in.nextInt();
        int d=in.nextInt();
        int[] a=new int[n];
        for(int i=0; i<n; i++){
            a[i]=in.nextInt();
        }
        for(int i=0; i<n; i++){
        		System.out.print(a[i]+" ");
        }
        System.out.println();
        for(int i=0; i<d; i++)
        a=copy(a);
        for(int i=0; i<n; i++){
    		System.out.print(a[i]+" ");
    }
	}
	public static int[] copy(int[] a){
		int t=a[0];
		for(int i=0; i<a.length-1; i++){
			a[i]=a[i+1];
		}
		a[a.length-1]=t;
		return a;
	}

}
