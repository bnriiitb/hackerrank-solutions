
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class EqualizeTheArray {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int c=0;
        int a[] = new int[n];
        HashMap<Integer,Integer> h=new HashMap<>();
        for(int i=0; i < n; i++){
        		int t= in.nextInt();
        		if(h.get(t)==null)
        			h.put(t, 1);
        		else{
        			int k=h.get(t)+1;
        			h.put(t, k);
        		}
        }
        Set<Integer> keys=h.keySet();
        int i=0;
        a=new int [keys.size()];
        for(Integer k: keys){
        		a[i]=h.get(k);
        		i++;
        }
        Arrays.sort(a);
        for(int j=a.length-2; j>=0; j--){
        		c+=a[j];
        }
        System.out.println(c);
        }

}
